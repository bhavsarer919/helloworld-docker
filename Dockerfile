FROM openjdk:8u181-jre

WORKDIR /app

ADD target/helloworld-*.jar app.jar

CMD [ "java", "-Djava.security.egd=file:/dev/./urandom","-jar","app.jar"]