package com.example.helloworlddocker;

import com.sforce.soap.enterprise.EnterpriseConnection;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class HelloworldDockerApplication {

	public static void main(String[] args) {
		SpringApplication.run(HelloworldDockerApplication.class, args);
	}

	@GetMapping("/helloworld")
	public String helloWorld() {
		EnterpriseConnection connection = null;
		return "Hello World !!! " + null;
	}

}
